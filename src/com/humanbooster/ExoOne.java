package com.humanbooster;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExoOne {
    public static void main(String[] args) {
        // Je demande à l'utilisateur de saisir un nombre
        System.out.println("Veuillez saisir un nombre");
        // Je cré un nouvel objet scanner qui permettra de lire les entrées
        // utilisateur dans la console
        Scanner scanner = new Scanner(System.in);
        // Je déclare une variable de type Integer (wrapper)
        // qui contiendra la saisie utilisateur
        Integer number;
        try{
            // Je réccupére la saisie utilisateur dans la console
            number = scanner.nextInt();
            // Si le nombre est supérieur à 0, il est positif
            if(number < 0){
                System.out.println("Le nombre est négatif");
                // Sinon, il est négatif
            } else {
                System.out.println("Le nombre est positif");
            }
        }
        // Si l'utilisateur n'a pas saisie un nombre, je passe dans ce cas
        // Je lui affiche qu'il n'a pas lu les consignes
        catch (InputMismatchException e){
            System.out.println("J'avais demandé un nombre ...");
        }
    }
}

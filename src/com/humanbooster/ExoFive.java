package com.humanbooster;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExoFive {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Veuillez saisir la devise d'entrée (EUR, USD, BTC, ETH)");
        String entree = scanner.nextLine();

        System.out.println("Veuillez saisir la devise de sortie (EUR, USD, BTC, ETH)");
        String sortie = scanner.nextLine();

        System.out.println("Veuillez saisir le montant à convertir");
        Double montant;
        Double conversion = 0.00;

        try {
             montant = scanner.nextDouble();
        } catch (InputMismatchException e) {
            System.out.println("Vous n'avez pas saisie un montant valide");
            return;
        }


        if(entree.equals("EUR")){
            if(sortie.equals("EUR")){
                conversion = montant;
            } else if (sortie.equals("USD")){
                conversion = montant*1.0858;
            } else if (sortie.equals("BTC")){
                conversion = montant*0.000036;
            } else if (sortie.equals("ETH")){
                conversion = montant*0.00058;
            }
        }

        if(entree.equals("USD")){
            if(sortie.equals("USD")){
                conversion = montant;
            } else if (sortie.equals("EUR")){
                conversion = montant*0.9210;
            } else if (sortie.equals("BTC")){
                conversion = montant*0.000033;
            } else if (sortie.equals("ETH")){
                conversion = montant*0.00053;
            }
        }

        if(entree.equals("BTC")){
            if(sortie.equals("BTC")){
                conversion = montant;
            } else if (sortie.equals("EUR")){
                conversion = montant*27719.14;
            } else if (sortie.equals("USD")){
                conversion = montant*30048.50;
            } else if (sortie.equals("ETH")){
                conversion = montant*16.12;
            }
        }

        if(entree.equals("ETH")){
            if(sortie.equals("ETH")){
                conversion = montant;
            } else if (sortie.equals("EUR")){
                conversion = montant*1729.52;
            } else if (sortie.equals("USD")){
                conversion = montant*1874.86;
            } else if (sortie.equals("BTC")){
                conversion = montant*0.062;
            }
        }

        double arondi = (double) Math.round(conversion*100.00)/100.00;

        System.out.println("La conversion donne " + arondi + " "+ sortie);

    }
}

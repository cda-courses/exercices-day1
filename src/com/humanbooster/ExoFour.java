package com.humanbooster;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExoFour {
    public static void main(String[] args){
        /*
         * Saisie du premier nombre
         */
        System.out.println("Veuillez saisir le premier nombre");
        Scanner scanner = new Scanner(System.in);
        Integer number1;
        try{
            number1 = scanner.nextInt();
        } catch (InputMismatchException e){
            System.out.println("Ce n'est pas un nombre");
            return;
        }

        /*
         * Saisie du second nombre nombre
         */
        System.out.println("Veuillez saisir le deuxième nombre");

        Integer number2;
        try{
            // Je réccupére la saisie utilisateur dans la console
            number2 = scanner.nextInt();
        } catch (InputMismatchException e){
            System.out.println("Ce n'est pas un nombre");
            return;
        }

        /*
         * Saisie du second nombre nombre
         */
        System.out.println("Veuillez saisir le troisième nombre");

        Integer number3;
        try{
            // Je réccupére la saisie utilisateur dans la console
            number3 = scanner.nextInt();
        } catch (InputMismatchException e){
            System.out.println("Ce n'est pas un nombre");
            return;
        }

        double somme = number1 + number2 + number3;
        double moyenne = somme / 3;

       if(moyenne<10){
           System.out.println("Insuffisant");
       } else if(moyenne<12){
           System.out.println("Passable");
       } else if (moyenne<14){
           System.out.println("Assez bien");
       } else if (moyenne<16){
           System.out.println("Bien");
       } else {
           System.out.println("Très bien");
       }
    }
}

package com.humanbooster;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExoThree {
    public static void main(String[] args){
        // Je cré un objet scanner pour lire dans la console
        Scanner scanner = new Scanner(System.in);
        // Je demande à l'utilisateur de saisir un nombre
        System.out.println("Veuillez saisir un nombre !");
        // Je déclare une valeur nombre que j'utiliserais plus tard
        int nombre;

        // J'essaie de faire quelque chose
        try{
             // J'essaie de réccupérer le nombre saisie dans la console
             nombre = scanner.nextInt();
             // Je n'y suis pas arrivé car je n'ai pas saisi un nombre
        } catch (InputMismatchException e){
            // Je dis à l'utilisateur qu'il n'a pas saisie un nombre
            System.out.println("J'ai demandé un nombre ...");
            // Je termine le programme !
            return;
        }

        // Si le reste de mon nombre divisé par 2 est égal à 0
        if(nombre%2 == 0){
            // J'informe mon utilisateur que le nombre est pair !
            System.out.println("Mon nombre est pair");
        } else{
            // Sinon, j'informe mon utilisateur que le nombre est impair !
            System.out.println("Mon nombre est impair");
        }

    }
}

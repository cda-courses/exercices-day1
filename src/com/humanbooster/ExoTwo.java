package com.humanbooster;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExoTwo {
    public static void main(String[] args){
        // Je cré un nouvel objet de type scanner
        // Il me servira à lire les entrées utilisateur
        Scanner scanner = new Scanner(System.in);

        // Je demande à l'utilisateur de saisir un nombre
        System.out.println("Veuillez saisir le nombre 1");

        // Je déclare 2 variables qui stockeront
        // les 2 nombres saisies par mon utilisateur
        int nombre1, nombre2;

        // J'essaie de réccupérer le nombre saisie par l'utilisateur
        try {
            // Si j'y arrive je le stocke dans nombre 1
            nombre1 = scanner.nextInt();

            // Je demande à l'utilisateur de saisir le nombre 2
            System.out.println("Veuillez saisir le nombre 2");
            // J'essaie de réccupérer le nombre 2

        } catch (InputMismatchException e){
            System.out.println("La première saisie n'est pas un nombre");
            return;
        }

        try {
            // Le cas ou j'y arrive, je le stocke dans la variable nombre 2
            nombre2 = scanner.nextInt();

            // Je compare les deux nombres
            if(nombre1>nombre2){
                // J'affiche ici que le nombre 1 est supérieur
                System.out.println("Le nombre 1 ("+nombre1+") est supérieur au nombre 2 ("+nombre2+")");
            } else if(nombre2>nombre1){
                // J'affiche si le nombre 2 est supérieur
                System.out.println("Le nombre 2 ("+nombre2+") est supérieur au nombre 1 ("+nombre1+")");
            } else {
                // J'affiche si les nombres sont égaux
                System.out.println("Les nombres sont égaux ("+nombre1+")");
            }
            // Le cas ou la saisie du nombre 2 n'est pas un nombre
        } catch (InputMismatchException e){
            System.out.println("La deuxième saisie n'est pas un nombre");
            return;
        }
    }
}
